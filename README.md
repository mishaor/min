# Min

A minimalistic Twitter client. Written in React + TypeScript.  
This project is currently a non-functioning **stub**.

## Contributing

Make sure you have Node.js 11.x and Yarn installed.
Then:

```
git clone https://gitlab.com/mishaor/min && cd min
yarn start
```

and let it run in background (e.g. a terminal).  
You can start coding now. The app reloads on changes,  
so you don't need to rebuild it every time.
