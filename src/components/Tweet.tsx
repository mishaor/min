import * as React from 'react';

export interface TweetProps { key: number, author: string, content: string };

export const Tweet = (props: TweetProps) => {
    return (<div style={{margin: '5px', border: '5px', borderColor: 'black', borderStyle: 'solid'}}>
        <span>{props.author}</span>
        <p>
            {props.content}    
        </p>
    </div>);
};