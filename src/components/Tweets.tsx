import * as React from 'react';

import { Tweet } from './Tweet';

export const Tweets = () => {
    const tweetList = [
        { 
            id: 0, 
            author: 'actual_mishaor', 
            content: 'heya just checking my min thing' 
        },
        {
            id: 1,
            author: 'actual_mishaor',
            content: 'note to self: implement actual twitter stuff'
        }]
    const tweetCompList = tweetList.map((elem) => <Tweet key={elem.id} author={elem.author} content={elem.content} />);

    return <div>{tweetCompList}</div>;
};