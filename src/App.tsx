import * as React from 'react';

import { Tweets } from './components/Tweets';

export const App = () => {
    return (
    <>
        <Tweets />
    </>);
};