export interface ITweet {
    author: string,
    content: string
};