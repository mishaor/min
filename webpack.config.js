const path = require('path');

const htmlPlugin = require('html-webpack-plugin');
const cleanPlugin = require('clean-webpack-plugin');

module.exports = {
    entry: './src/index.tsx',
    mode: 'development',
    devtool: 'source-map',
    output: {
        filename: '[name].[hash].js',
        path: path.resolve(__dirname, 'app')
    },
    module: {
        rules: [
            { test: /\.ts(x)?$/, use: 'awesome-typescript-loader' },
            { test: /\.js$/, use: 'source-map-loader',
            enforce: 'pre' }
        ]
    },
    resolve: {
        extensions: [ '.tsx', '.ts', '.js', '.json' ]
    },
    plugins: [
        new htmlPlugin({
            template: 'src/template.html'
        }),
        new cleanPlugin('app')
    ]
}